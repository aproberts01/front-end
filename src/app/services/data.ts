export interface Data {
  classification: string;
  ctq: boolean;
  isQualified: boolean;
  jurisdiction: string;
  lastUpdatedBy: string;
  lastUpdatedDate: string;
  openPo: boolean;
  partName: string;
  partNumber: string;
  qplExpirationDate: string;
  revision: string;
  supplierCode: string;
  supplierName: string;
  toolDieSetNumber: string;
}

