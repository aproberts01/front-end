import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class ApiService {
  API_KEY: string = "3D9BBDEB-CB28-4724-A392-00EBF084EAF7";
  offset: number = 0;
  pageSize: number = 5;
  URL: string = "https://exam.net-inspect.com/qpl?offset=";

  constructor(private httpClient: HttpClient) {}

  public getData(urlParams: string) {
    const headers = {
      "Content-Type": "application/json",
      "Authorization": `${this.API_KEY}`
    };

    const requestOptions = {
      headers: new HttpHeaders(headers)
    };

    return this.httpClient.get(this.URL + urlParams, requestOptions);
  }
}
