import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { SearchbarComponent } from "./components/searchbar/searchbar.component";
import { TableComponent } from "./components/table/table.component";
import { FooterComponent } from "./components/footer/footer.component";

import { HttpClientModule } from "@angular/common/http";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faCoffee,
  faSearch,
  faStepForward,
  faStepBackward,
  faFastForward,
  faFastBackward
} from "@fortawesome/free-solid-svg-icons";

library.add(
  faCoffee,
  faSearch,
  faStepForward,
  faStepBackward,
  faFastForward,
  faFastBackward
);

@NgModule({
  declarations: [
    AppComponent,
    SearchbarComponent,
    TableComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FontAwesomeModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
