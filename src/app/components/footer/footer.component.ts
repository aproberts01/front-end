import { ApiService } from "./../../services/api.service";
import { Component, OnInit, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "app-footer",
  templateUrl: "./footer.component.html",
  styleUrls: ["./footer.component.scss"]
})
export class FooterComponent {
  public offset: number = 0;
  public maxPage: number = 100;
  public itemsPerPage: number = 10;
  public iconArr: string[] = [
    "fast-backward",
    "step-backward",
    "step-forward",
    "fast-forward"
  ];

  @Output() public itemsPerPageChange = new EventEmitter();
  @Output() public changePage = new EventEmitter();

  constructor(private apiService: ApiService) {}

  select(e) {
    // @ts-ignore
    this.itemsPerPage = parseInt(e.target.value);
    this.itemsPerPageChange.emit(this.itemsPerPage);
  }

  changePageNumber(e) {
    // @ts-ignore
    let number = parseInt(e.currentTarget.id);

    let maxEntries = 100;
    let maxOffset = maxEntries - this.itemsPerPage;
    switch (number) {
      case 1:
        // @ts-ignore
        this.offset = Math.max(this.offset - this.itemsPerPage, 0);
        break;
      case 2:
        // @ts-ignore
        this.offset = Math.min(this.offset + this.itemsPerPage, maxOffset);
        break;
      case 3:
        this.offset = maxEntries - this.itemsPerPage;
        break;
      default:
        this.offset = 0;
    }
    this.changePage.emit(this.offset);
  }
}
