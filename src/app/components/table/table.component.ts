import { Component, OnInit, Input } from "@angular/core";
import { ApiService } from "src/app/services/api.service";
import { Data } from "../../services/data";
import { headers } from "./headers";

@Component({
  selector: "app-table",
  templateUrl: "./table.component.html",
  styleUrls: ["./table.component.scss"]
})
export class TableComponent {
  public data: Data[] = [];
  // @ts-ignore
  public headerKeys = Object.keys(headers);
  // @ts-ignore
  public headerValues = Object.values(headers);
  public resultsFound = true;

  @Input() public pageSize = 9;
  @Input() public offset = 0;
  @Input() public searchValue = "";
  constructor(private api: ApiService) {}

  ngOnChanges() {
    this.getData();
  }

  getData() {
    this.api
      .getData(`${this.offset}&pageSize=${this.pageSize}`)
      .subscribe(data => {
        this.data = data;
        this.filterTable(this.searchValue);
      });
  }

  filterTable(val) {
    if (val) {
      let value = val.toLowerCase().toString();
      //@ts-ignore
      let filteredData = this.data.filter((item, i) => {
        //@ts-ignore
        return Object.keys(item).some(key => {
          return item[key]
            .toString()
            .toLowerCase()
            .includes(value);
        });
      });
      this.data = filteredData;
    }
    //@ts-ignore
    this.data.length === 0 ? this.resultsFound = false : this.resultsFound = true;
  }

  handleValues(val) {
    if (typeof val === "boolean") {
      return val === true ? "Yes" : "No";
    }
    return val;
  }
}
