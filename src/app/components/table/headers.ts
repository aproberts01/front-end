export const headers = {
  "Part Number": "partNumber",
  "Part Revision": "revision",
  "Part Name": "partName",
  "Tool / Die Set Number": "toolDieSetNumber",
  "QPL": "isQualified",
  "Open PO": "openPo",
  "Part Jurisdiction": "jurisdiction",
  "Part Classification": "classification",
  "Supplier Company Name": "supplierName",
  "Supplier Company Code": "supplierCode",
  "CTQ": "ctq",
  "QPL Last Updated By": "lastUpdatedBy",
  "QPL Last Updated Date": "lastUpdatedDate"
};
