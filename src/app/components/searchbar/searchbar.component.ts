import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { faSearch } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: "app-searchbar",
  templateUrl: "./searchbar.component.html",
  styleUrls: ["./searchbar.component.scss"]
})
export class SearchbarComponent {

  faSearch = faSearch;
  searchValue: string = ''

  @Output() public sendSearchValue = new EventEmitter()

  sendSearch(){
    this.sendSearchValue.emit(this.searchValue)
  }

}
