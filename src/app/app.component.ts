import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {
  title = "View QPL List";

  public pageSize: number = 10;
  public offset: number = 0;
  public searchValue: string = ''

  change(e) {
    this.pageSize = e;
  }

  changePage(e) {
    this.offset = e;
  }

  sendSearch(e) {
    this.searchValue = e
  }
}
